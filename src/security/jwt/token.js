const jwt = require('jsonwebtoken');

const {
    ServerError
} = require('../../errors');

const {
    getSecret
} = require('docker-secret');

const generalOptions = {
    issuer: 'Workshop Moby',
    subject: 'Authorization Token Workshop Moby',
    audience: 'Workshop Moby Users'
};

const jwtKey = process.env.NODE_ENV === 'development' ? process.env.JWT_SECRET_KEY : getSecret(process.env.JWT_SECRET_KEY_FILE);

const generateToken = async (payload) => {
    try {
        const encodingOptions = { ...generalOptions, expiresIn: process.env.JWT_EXPIRE_TIME || '1h' }
        const token = await jwt.sign(payload.toPlainObject(), jwtKey, encodingOptions);
        return token;
    } catch (err) {
        console.error(err);
        throw new ServerError("Error when encoding the token!", 500);
    }
};

const verifyAndDecodeData = async (token, ignoreExpiration = false) => {
    try {
        const decodingOptions = {...generalOptions, ignoreExpiration}
        const decoded = await jwt.verify(token, jwtKey, decodingOptions);
        return decoded;
    } catch (err) {
        if (err.message === 'jwt expired') {
            throw new ServerError('Token is expired. Please refresh!', 401);
        } else if (err.message === 'jwt malformed') {
            throw new ServerError('Token is malformed!', 401);
        }
        throw new ServerError("Token is compromised!", 401);
    }
};


module.exports = {
    generateToken,
    verifyAndDecodeData
};
