const Router = require('express').Router();

const {
    ServerError
} = require('../errors');

const {
    register,
    login,
    remove
} = require('./services.js');

const {
    authorizeAndExtractToken
} = require('../security/jwt/filters.js');

Router.post('/', async (req, res) => {
    const {
        email,
        password
    } = req.body;

    if (typeof email !== 'string' || email.length < 3 || !email.includes('@')) {
        throw new ServerError('Invalid email format', 400);
    }

    if (typeof password !== 'string' || password.length < 4) {
        throw new ServerError('Invalid password format', 400);
    }

    const token = await register(email, password);

    res.json({
        token
    });
});

Router.post('/login', async (req, res) => {
    const {
        email,
        password
    } = req.body;

    if (typeof email !== 'string' || email.length < 3 || !email.includes('@')) {
        throw new ServerError('Invalid email format', 400);
    }

    if (typeof password !== 'string' || password.length < 4) {
        throw new ServerError('Invalid password format', 400);
    }

    const token = await login(email, password);

    res.json({
        token
    });

});


Router.delete('/', authorizeAndExtractToken, async (req, res) => {
    const {
        userId
    } = req.state.decoded;

    await remove(userId);

    res.status(200).end();
});

module.exports = Router;