const {
    Pool
  } = require('pg');
  
  const {
    getSecret
  } = require('docker-secret');
  

console.log(process.env.PGUSER_FILE);
console.log(process.env.PGPASSWORD_FILE);

  const options = {
    host: process.env.PGHOST,
    database: process.env.PGDATABASE,
    port: process.env.PGPORT,
    user: process.env.NODE_ENV === 'development' ? process.env.PGUSER : getSecret(process.env.PGUSER_FILE),
    password: process.env.NODE_ENV === 'development' ? process.env.PGPASSWORD : getSecret(process.env.PGPASSWORD_FILE)
  }

  console.log(options);
  
  const pool = new Pool(options);
  
  const query = async (text, params) => {
    const {
      rows,
    } = await pool.query(text, params);
    return rows;
  };
  
  module.exports = {
    query,
  };